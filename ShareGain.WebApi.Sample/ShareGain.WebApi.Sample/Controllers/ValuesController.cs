﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace ShareGain.WebApi.Sample.Controllers
{
    public class EventDetails
    {
        public string Code { get; set; }
        public string FullName { get; set; }

    }

    public class DelteCodeRequest
    {
        public string Code { get; set; }
      

    }


    public class NodeConfiguration
    {
        
        public string HostName { get; set; }
        public string IpAddress { get; set; }
        public string Status { get; set; }
        public IEnumerable<string> SecurityGroups { get; set; }
        public IEnumerable<string> HostedApplication { get; set; }
        public string AvailbleMemory { get; set; }
        public string CpuUsage { get; set; }
    }


    public class ClusterConfig
    {
        public string ClusterName { get; set; }
        public string ResourceGroup  { get; set; }
        public IDictionary<string,NodeConfiguration> Nodes { get; set; }

    }

    public class AddNodeRequest
    {
        public string HostName { get; set; }
        public string IpAddress { get; set; }
    }

    public class DeleteNodeReq
    {
        public string NodeId { get; set; }

    }
    public class ConfigurationSettings
    {
        private static Lazy<IDictionary<string,NodeConfiguration>> NodesInfo = new Lazy<IDictionary<string,NodeConfiguration>>(() => 
        {
            var nodesInfo = new Dictionary<string,NodeConfiguration>();
            nodesInfo.Add(Guid.NewGuid().ToString(),new NodeConfiguration()
            {
                AvailbleMemory = "1.2 GB",
                CpuUsage = "45%",
                HostName = "Sharegain-Web-Node1",
                IpAddress = "142.154.11.2",
                Status = "Running",
                SecurityGroups = new List<string>() { "CoreServices" },
             
                HostedApplication = new List<string>() { "SG-webapi", "Sg-portal"}
            });


               
            nodesInfo.Add(Guid.NewGuid().ToString(),new NodeConfiguration()
            {
                AvailbleMemory = "1.8 GB",
                CpuUsage = "87%",
                HostName = "Sharegain-Web-Node2",
                IpAddress = "142.154.11.3",
                Status = "Running",
                SecurityGroups = new List<string>() { "CoreServices","SupportServices" },
               
                HostedApplication = new List<string>() { "SG-core", "Sg-price-engine" }
            });

                
            nodesInfo.Add(Guid.NewGuid().ToString(),new NodeConfiguration()
            {
                AvailbleMemory = "2.7 GB",
                CpuUsage = "90%",
                HostName = "Sharegain-Web-Node3",
                IpAddress = "142.154.11.4",
                Status = "Running",
                SecurityGroups = new List<string>() { "CoreServices","InternalServices" },
                HostedApplication = new List<string>() { "SG-core", "Sg-price-engine" }
            });


            nodesInfo.Add(Guid.NewGuid().ToString(),new NodeConfiguration()
            {
                AvailbleMemory = "0.2 GB",
                CpuUsage = "45%",
                HostName = "Sharegain-Web-Node4",
                IpAddress = "142.154.11.5",
                Status = "Unreachable",
                SecurityGroups = new List<string>() { "CoreServices" },
                HostedApplication = new List<string>() { "SG-core", "Sg-Matching-engine" }
            });


            nodesInfo.Add(Guid.NewGuid().ToString(),new NodeConfiguration()
            {
                AvailbleMemory = "5 GB",
                CpuUsage = "22%",
                HostName = "Sharegain-Web-Node5",
                IpAddress = "142.154.11.6",
                Status = "Running",
                SecurityGroups = new List<string>() { "CoreServices" },
               
                HostedApplication = new List<string>() { "SG-core", "Sg-Loan-Engine" }
            });

            return nodesInfo;

        });

        public static void AddNodeToConfig(AddNodeRequest node)
        {
            NodesInfo.Value.Add(Guid.NewGuid().ToString(),new NodeConfiguration() {HostName =node.HostName,IpAddress =node.IpAddress });
        }

        public static void RemoveNode(string nodeUniqueId)
        {
            if (NodesInfo.Value.Keys.Contains(nodeUniqueId))
            {
                NodesInfo.Value.Remove(nodeUniqueId);
            }

            else
            {
                throw new Exception("Cant delete non existing node");
            }
        }

        public static IDictionary<string,NodeConfiguration> GetConfig()
        {
            return NodesInfo.Value;
        }

    }

    public static class EventsMap
    {
        private static Lazy<Dictionary<string, string>> events1 =
        new Lazy<Dictionary<string, string>>(() => 
        {
            var events = new Dictionary<string, string>();
            events.Add("AGM", "Annual General Meeting");
            events.Add("AGNCY", "Agency");
            events.Add("AGYDT", "Agency Details");
            events.Add("ANN", "Announcement");
            events.Add("ARR", "Arrangement");
            events.Add("ASSM", "Assimilation");
            events.Add("AUCT", "Auction");
            events.Add("BB", "Buyback");
            events.Add("BBC", "Bloomberg Composite ID");
            events.Add("BBE", "Bloomberg Exchange ID");
            events.Add("BBEC", "Bloomberg Exchange ID Change");
            events.Add("BDC", "Business Day Convention Center");
            events.Add("BHM", "Bond Holder Meeting");
            events.Add("BKRP", "Bankruptcy");
            events.Add("BKRPNOTES", "Bankruptcy Notes");
            events.Add("BNDLQ", "Bond Liquidation");
            events.Add("BOCHG", "Bond O/s Change");
            events.Add("BON", "Bonus");
            events.Add("BOND", "Bond");
            events.Add("BR", "Bonus Rights");
            events.Add("BSCHG", "Bond Static Change");
            events.Add("BSKCC", "Basket Warrant Constituent Change");
            events.Add("CALL", "Call");
            events.Add("CALLPUT", "Call-Put");
            events.Add("CANCEL", "Cancel");
            events.Add("CAPRD", "Capital Reduction");
            events.Add("CH144", "Link 144A Change");
            events.Add("CLACT", "Class Action");
            events.Add("CLEAN", "Clean");
            events.Add("CLSAC", "Class Action");
            events.Add("CMACQ", "Compulsory Acquisition");
            events.Add("CNTR", "Center");
            events.Add("CONSD", "Consolidation");
            events.Add("CONV", "Conversion");
            events.Add("CONVT", "Conversion Terms");
            events.Add("CORR", "Correction");
            events.Add("COSNT", "Consent");
            events.Add("CPOPN", "Call Put Option Notes");
            events.Add("CPOPT", "Schedule");
            events.Add("CRCHG", "Credit Rating Change");
            events.Add("CRDRT", "Credit Ratings");
            events.Add("CTCHG", "Conversion Terms Change");
            events.Add("CTX", "Certificate of Exchange");
            events.Add("CURRD", "Currency Redenomination");
            events.Add("DEFUNCT", "Defunct SEDOL");
            events.Add("DIST", "Distribution");
            events.Add("DIV", "Dividend");
            events.Add("DMRGR", "Demerger");
            events.Add("DRCHG", "Depository Receipt Change");
            events.Add("DRIP", "Dividend Reinvestment Plan");
            events.Add("DVST", "Divestment");
            events.Add("ENT", "Entitlement Issue");
            events.Add("EXCHG", "Exchange");
            events.Add("EXOFF", "Exchange Offer");
            events.Add("FI", "Further Issue");
            events.Add("FRANK", "Franking");
            events.Add("FRNFX", "FRN Fixing");
            events.Add("FYCHG", "Financial Year Change");
            events.Add("ICC", "International Code Change");
            events.Add("IFCHG", "Interest Frequency Change");
            events.Add("INCHG", "Country of Incorporation Change");
            events.Add("INDEF", "In Default");
            events.Add("INT", "Interest");
            events.Add("INTBC", "Interest Basis Change");
            events.Add("INTPY", "Interest Payment Announcement");
            events.Add("IRCHG", "Interest Rate Change");
            events.Add("ISCHG", "Issuer Name Change");
            events.Add("ISSDD", "Issuer Debt Default");
            events.Add("ISSUR", "Issuer");
            events.Add("LAWST", "Lawsuit");
            events.Add("LAWSTNOTES", "Lawsuit Notes");
            events.Add("LCC", "Local Code Change");
            events.Add("LIQ", "Liquidation");
            events.Add("LIQNOTES", "Liquidation Notes");
            events.Add("LSTAT", "Listing Status");
            events.Add("LTCHG", "Lot Change");
            events.Add("MIFID", "MIFID Securities");
            events.Add("MKCHG", "Market Segment Change");
            events.Add("MKTSG", "Market Segment");
            events.Add("MLC", "Market Lot Change");
            events.Add("MPAY", "Multiple Payment");
            events.Add("MRGR", "Merger");
            events.Add("MTCHG", "Maturity Date Change");
            events.Add("NC", "Name Change");
            events.Add("NLIST", "New Listing");
            events.Add("NS", "Not Specified");
            events.Add("ODDLT", "Odd Lot Offer");
            events.Add("OEVNT", "Other Event");
            events.Add("PID", "Property Income Disribution");
            events.Add("PIK", "Pay In Kind");
            events.Add("PO", "Purchase Offer");
            events.Add("PRCHG", "Primary Exchange Change");
            events.Add("PRF", "Preferential Offer");
            events.Add("PRFRD", "Preferential Redemption");
            events.Add("PUT", "Exercise of Put Option");
            events.Add("PVRD", "Par Value Redenomination");
            events.Add("RCAP", "Return of Capital");
            events.Add("RCONV", "Reconvention");
            events.Add("RDNOM", "Redenomination");
            events.Add("REDEM", "Redemption");
            events.Add("REDMT", "Redemption Terms");
            events.Add("REISS", "Reissuance");
            events.Add("RESTORE", "Restore Defunct Sedol");
            events.Add("RTRAC", "Retractions");
            events.Add("RTS", "Rights");
            events.Add("SACHG", "Security Agency Change");
            events.Add("SCAGY", "Security Agency");
            events.Add("SCCHG", "Security Name Change");
            events.Add("SCEXH", "Security Exchange");
            events.Add("SCMST", "Security Master");
            events.Add("SCSWP", "Security Swap");
            events.Add("SCXTC", "Security Exchange Trading Currency");
            events.Add("SD", "Sub Division");
            events.Add("SDCHG", "Sedol Change");
            events.Add("SECRC", "Security Re-classification");
            events.Add("SEDOL", "SEDOL");
            events.Add("SELRT", "Selling Restrictions");
            events.Add("SFUND", "Sinking Fund");
            events.Add("SHOCH", "Shares Outstanding Change");
            events.Add("SXTCH", "Security Exchange Trading Currency Change");
            events.Add("TENDER", "Tender Offer");
            events.Add("TKOVR", "Takeover");
            events.Add("TLELK", "Temp Line Security");
            events.Add("TRCHG", "Treasury Shares Outstanding Change");
            events.Add("TRNCH", "Tranche");
            events.Add("UNC", "Umbrella Programme Name Change");
            events.Add("UNSTP", "Unstaple");
            events.Add("WKNCH", "WKN Change");
            events.Add("WTCHG", "Warrant Terms Change");
            events.Add("WXCHG", "Warrant Exercise Change");
            return events;


        });
        public static Dictionary<string, string> GetMap()
        {
            return events1.Value;
        }

    }

    public class ValuesController : ApiController
    {
        // GET api/values
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/EventsMapping")]
        public Dictionary<string,string> Get()
        {

            var header = Request.Headers.GetValues("token").Single().ToString();
            if (string.IsNullOrEmpty(header))
            {
                throw new Exception("Missing token");
            }

            if (header != "0c5701e8-bc29-45ac-9977-f12a3133f5ce")
            {
                throw new Exception("Token Invalid");
            }

            return EventsMap.GetMap();


            //var events = new Dictionary<string, string>();
            //events.Add("AGM", "Annual General Meeting");
            //events.Add("AGNCY", "Agency");
            //events.Add("AGYDT", "Agency Details");
            //events.Add("ANN", "Announcement");
            //events.Add("ARR", "Arrangement");
            //events.Add("ASSM", "Assimilation");
            //events.Add("AUCT", "Auction");
            //events.Add("BB", "Buyback");
            //events.Add("BBC", "Bloomberg Composite ID");
            //events.Add("BBE", "Bloomberg Exchange ID");
            //events.Add("BBEC", "Bloomberg Exchange ID Change");
            //events.Add("BDC", "Business Day Convention Center");
            //events.Add("BHM", "Bond Holder Meeting");
            //events.Add("BKRP", "Bankruptcy");
            //events.Add("BKRPNOTES", "Bankruptcy Notes");
            //events.Add("BNDLQ", "Bond Liquidation");
            //events.Add("BOCHG", "Bond O/s Change");
            //events.Add("BON", "Bonus");
            //events.Add("BOND", "Bond");
            //events.Add("BR", "Bonus Rights");
            //events.Add("BSCHG", "Bond Static Change");
            //events.Add("BSKCC", "Basket Warrant Constituent Change");
            //events.Add("CALL", "Call");
            //events.Add("CALLPUT", "Call-Put");
            //events.Add("CANCEL", "Cancel");
            //events.Add("CAPRD", "Capital Reduction");
            //events.Add("CH144", "Link 144A Change");
            //events.Add("CLACT", "Class Action");
            //events.Add("CLEAN", "Clean");
            //events.Add("CLSAC", "Class Action");
            //events.Add("CMACQ", "Compulsory Acquisition");
            //events.Add("CNTR", "Center");
            //events.Add("CONSD", "Consolidation");
            //events.Add("CONV", "Conversion");
            //events.Add("CONVT", "Conversion Terms");
            //events.Add("CORR", "Correction");
            //events.Add("COSNT", "Consent");
            //events.Add("CPOPN", "Call Put Option Notes");
            //events.Add("CPOPT", "Schedule");
            //events.Add("CRCHG", "Credit Rating Change");
            //events.Add("CRDRT", "Credit Ratings");
            //events.Add("CTCHG", "Conversion Terms Change");
            //events.Add("CTX", "Certificate of Exchange");
            //events.Add("CURRD", "Currency Redenomination");
            //events.Add("DEFUNCT", "Defunct SEDOL");
            //events.Add("DIST", "Distribution");
            //events.Add("DIV", "Dividend");
            //events.Add("DMRGR", "Demerger");
            //events.Add("DRCHG", "Depository Receipt Change");
            //events.Add("DRIP", "Dividend Reinvestment Plan");
            //events.Add("DVST", "Divestment");
            //events.Add("ENT", "Entitlement Issue");
            //events.Add("EXCHG", "Exchange");
            //events.Add("EXOFF", "Exchange Offer");
            //events.Add("FI", "Further Issue");
            //events.Add("FRANK", "Franking");
            //events.Add("FRNFX", "FRN Fixing");
            //events.Add("FYCHG", "Financial Year Change");
            //events.Add("ICC", "International Code Change");
            //events.Add("IFCHG", "Interest Frequency Change");
            //events.Add("INCHG", "Country of Incorporation Change");
            //events.Add("INDEF", "In Default");
            //events.Add("INT", "Interest");
            //events.Add("INTBC", "Interest Basis Change");
            //events.Add("INTPY", "Interest Payment Announcement");
            //events.Add("IRCHG", "Interest Rate Change");
            //events.Add("ISCHG", "Issuer Name Change");
            //events.Add("ISSDD", "Issuer Debt Default");
            //events.Add("ISSUR", "Issuer");
            //events.Add("LAWST", "Lawsuit");
            //events.Add("LAWSTNOTES", "Lawsuit Notes");
            //events.Add("LCC", "Local Code Change");
            //events.Add("LIQ", "Liquidation");
            //events.Add("LIQNOTES", "Liquidation Notes");
            //events.Add("LSTAT", "Listing Status");
            //events.Add("LTCHG", "Lot Change");
            //events.Add("MIFID", "MIFID Securities");
            //events.Add("MKCHG", "Market Segment Change");
            //events.Add("MKTSG", "Market Segment");
            //events.Add("MLC", "Market Lot Change");
            //events.Add("MPAY", "Multiple Payment");
            //events.Add("MRGR", "Merger");
            //events.Add("MTCHG", "Maturity Date Change");
            //events.Add("NC", "Name Change");
            //events.Add("NLIST", "New Listing");
            //events.Add("NS", "Not Specified");
            //events.Add("ODDLT", "Odd Lot Offer");
            //events.Add("OEVNT", "Other Event");
            //events.Add("PID", "Property Income Disribution");
            //events.Add("PIK", "Pay In Kind");
            //events.Add("PO", "Purchase Offer");
            //events.Add("PRCHG", "Primary Exchange Change");
            //events.Add("PRF", "Preferential Offer");
            //events.Add("PRFRD", "Preferential Redemption");
            //events.Add("PUT", "Exercise of Put Option");
            //events.Add("PVRD", "Par Value Redenomination");
            //events.Add("RCAP", "Return of Capital");
            //events.Add("RCONV", "Reconvention");
            //events.Add("RDNOM", "Redenomination");
            //events.Add("REDEM", "Redemption");
            //events.Add("REDMT", "Redemption Terms");
            //events.Add("REISS", "Reissuance");
            //events.Add("RESTORE", "Restore Defunct Sedol");
            //events.Add("RTRAC", "Retractions");
            //events.Add("RTS", "Rights");
            //events.Add("SACHG", "Security Agency Change");
            //events.Add("SCAGY", "Security Agency");
            //events.Add("SCCHG", "Security Name Change");
            //events.Add("SCEXH", "Security Exchange");
            //events.Add("SCMST", "Security Master");
            //events.Add("SCSWP", "Security Swap");
            //events.Add("SCXTC", "Security Exchange Trading Currency");
            //events.Add("SD", "Sub Division");
            //events.Add("SDCHG", "Sedol Change");
            //events.Add("SECRC", "Security Re-classification");
            //events.Add("SEDOL", "SEDOL");
            //events.Add("SELRT", "Selling Restrictions");
            //events.Add("SFUND", "Sinking Fund");
            //events.Add("SHOCH", "Shares Outstanding Change");
            //events.Add("SXTCH", "Security Exchange Trading Currency Change");
            //events.Add("TENDER", "Tender Offer");
            //events.Add("TKOVR", "Takeover");
            //events.Add("TLELK", "Temp Line Security");
            //events.Add("TRCHG", "Treasury Shares Outstanding Change");
            //events.Add("TRNCH", "Tranche");
            //events.Add("UNC", "Umbrella Programme Name Change");
            //events.Add("UNSTP", "Unstaple");
            //events.Add("WKNCH", "WKN Change");
            //events.Add("WTCHG", "Warrant Terms Change");
            //events.Add("WXCHG", "Warrant Exercise Change");

            //return events;
        }

        [HttpPost]
        [System.Web.Http.Route("api/GetEndpointHealth")]
        public IHttpActionResult  CheckEndpointHealth()
        {
            return new StatusCodeResult(HttpStatusCode.OK, this);
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/AddEventToMapping")]
        public IHttpActionResult GenerateEventMap([FromBody]EventDetails req)
        {
            var header = Request.Headers.GetValues("token").Single().ToString();
            if (string.IsNullOrEmpty(header))
            {
                throw new Exception("Missing token");
            }

            if (header != "0c5701e8-bc29-45ac-9977-f12a3133f5ce")
            {
                throw new Exception("Token Invalid");
            }

            if (req.Code==null)
            {
                throw new ArgumentNullException();
            }

            if (req.FullName ==null)
            {
                throw new ArgumentNullException();
            }

            EventsMap.GetMap().Add(req.Code, req.FullName);
            return new StatusCodeResult(HttpStatusCode.OK, this);
        }


        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("api/GetClusterConfiguration")]
        public ClusterConfig GetConfig()
        {
            var header = Request.Headers.GetValues("token").Single().ToString();
            if (string.IsNullOrEmpty(header))
            {
                throw new Exception("Missing token");
            }

            if (header != "0c5701e8-bc29-45ac-9977-f12a3133f5ce")
            {
                throw new Exception("Token Invalid");
            }

            var config = ConfigurationSettings.GetConfig();
            return new ClusterConfig() { ClusterName = "Sharegain-Prod-Cluster", Nodes = config, ResourceGroup = "North-Europe" };
        }


        [HttpDelete]
        [System.Web.Http.Route("api/DeleteClusterNode")]
        public IHttpActionResult DeleteNode([FromBody]DeleteNodeReq req)
        {
            var header = Request.Headers.GetValues("token").Single().ToString();
            if (string.IsNullOrEmpty(header))
            {
                throw new Exception("Missing token");
            }

            if (header != "0c5701e8-bc29-45ac-9977-f12a3133f5ce")
            {
                throw new Exception("Token Invalid");
            }


            if (string.IsNullOrEmpty(req.NodeId))
            {
                throw new Exception("Node id is empty");
            }

            ConfigurationSettings.RemoveNode(req.NodeId);

            return new StatusCodeResult(HttpStatusCode.OK, this);
        }

        [HttpPost]
        [System.Web.Http.Route("api/AddClusterNode")]
        public IHttpActionResult AddNodeToCluster([FromBody] AddNodeRequest req)
        {
            var header = Request.Headers.GetValues("token").Single().ToString();
            if (string.IsNullOrEmpty(header))
            {
                throw new Exception("Missing token");
            }

            if (header != "0c5701e8-bc29-45ac-9977-f12a3133f5ce")
            {
                throw new Exception("Token Invalid");
            }


            if (string.IsNullOrEmpty(req.HostName))
            {
                throw new Exception("HostName is empty");
            }

            if (string.IsNullOrEmpty(req.IpAddress))
            {
                throw new Exception("Ip Address is empty");
            }

            if (ConfigurationSettings.GetConfig().Values.Where(x => x.IpAddress.ToLower() == req.IpAddress.ToLower()).Any())
            {
                throw new Exception("cand add a node because the cluster contains a node with the same ip address");
            }

            if (ConfigurationSettings.GetConfig().Values.Where(x => x.HostName.ToLower() == req.HostName.ToLower()).Any())
            {
                throw new Exception("cand add a node because the cluster contains a node with the same host name");
            }

            ConfigurationSettings.AddNodeToConfig(req);

            return new StatusCodeResult(HttpStatusCode.OK, this);
        }

        [System.Web.Http.HttpDelete]
        [System.Web.Http.Route("api/DeleteEventMapping")]
        public IHttpActionResult DeleteMapping([FromBody] DelteCodeRequest req)
        {

            var header = Request.Headers.GetValues("token").Single().ToString();
            if (string.IsNullOrEmpty(header))
            {
                throw new Exception("Missing token");
            }

            if (header != "0c5701e8-bc29-45ac-9977-f12a3133f5ce")
            {
                throw new Exception("Token Invalid");
            }
            var map = EventsMap.GetMap();
            if (!map.ContainsKey(req.Code))
            {
                return new StatusCodeResult(HttpStatusCode.NotFound, this);

            }

            else
            {
                map.Remove(req.Code);
                return new StatusCodeResult(HttpStatusCode.OK, this);

            }


        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/values/Testme")]
        public string sdfa()
        {
            return "222";
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("api/GenerateToken")]
        public string GetToken([FromBody]string value)
        {
            return "0c5701e8-bc29-45ac-9977-f12a3133f5ce";
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {

        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
